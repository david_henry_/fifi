package com.hackathon.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Dave on 12/5/13.
 */
public class Shelter implements Parcelable {

    private String name;
    private String phoneText;
    private String phone;
    private Double latitude;
    private Double longitude;

    public static final Shelter ANIMAL_FOUNDATION =
            new Shelter("The Animal Foundation", "(702) 384-3333", "7023843333", 36.176596 , -115.106574);
    public static final Shelter HENDERSON_CARE_AND_CONTROL =
            new Shelter("Henderson Animal Care and Control", "(702) 267-4970", "7022674970", 36.071302, -115.003191);
    public static final Shelter ADOPT_RESCUE_PET =
            new Shelter("Adopt A Rescue Pet", "(702) 798-8663", "7027988663", 36.101414, -115.132654);
    public static final Shelter WEIMARANER_CLUB =
            new Shelter("Las Vegas Weimaraner Club %26 Rescue", "(702) 280-6946", "7022806946", 36.108201, -115.10124);
    public static final Shelter LITTLE_FRIENDS =
            new Shelter("Little Friends Foundation", "(702) 463-9995", "7024639995", 36.242309, -115.089428);
    public static final Shelter NEVADA_SPCA =
            new Shelter("Nevada SPCA", "(702) 873-7722", "7028737722", 36.08874, -115.207316);
    public static final Shelter SECOND_CHANCE =
            new Shelter("Second Chance Animal Rescue", "(702) 885-9077", "7028859077", 36.192408, -115.304541);
    public static final Shelter SOUTHERN_NEVADA_BEAGLE =
            new Shelter("Southern Nevada Beagle Rescue Foundation", "(702) 493-9779", "7024939779", null, null);

    private static final ArrayList<Shelter> SHELTERS = new ArrayList<Shelter>() {{
        add(ANIMAL_FOUNDATION);
        add(HENDERSON_CARE_AND_CONTROL);
        add(ADOPT_RESCUE_PET);
        add(WEIMARANER_CLUB);
        add(LITTLE_FRIENDS);
        add(NEVADA_SPCA);
        add(SECOND_CHANCE);
        add(SOUTHERN_NEVADA_BEAGLE);
    }};

    public static Shelter ALL_LOCATIONS = null;

    public Shelter(String name, String phoneText, String phone, Double latitude, Double longitude) {
        this.name = name;
        this.phoneText = phoneText;
        this.phone = phone;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Shelter(Parcel in)
    {
        this.name = in.readString();
        this.phoneText = in.readString();
        this.phone = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
    }

    public String getPhoneText() {
        return phoneText;
    }

    public void setPhoneText(String phoneText) {
        this.phoneText = phoneText;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(name);
        dest.writeString(phoneText);
        dest.writeString(phone);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Shelter createFromParcel(Parcel in)
        {
            return new Shelter(in);
        }

        public Shelter[] newArray(int size)
        {
            return new Shelter[size];
        }
    };

    public static Shelter getShelterFromAnimal(String location) {
        for(Shelter shelter : SHELTERS) {
            if (shelter.getName().equals(location)) return shelter;
        }
        return null; // todo exception
    }
}
