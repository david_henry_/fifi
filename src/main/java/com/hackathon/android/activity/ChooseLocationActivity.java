package com.hackathon.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import com.hackathon.android.R;
import com.hackathon.android.model.Shelter;

/**
 * Created by Dave on 12/5/13.
 */
public class ChooseLocationActivity extends OptionsBarActivity {

    private Intent animalListIntent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.choose_location_view);

        initUIComponents();
    }

    private void initUIComponents() {

        animalListIntent = new Intent(this, AnimalListActivity.class);

        // Get the search type passed in from the previous activity and store it in the next activity
        String searchType = getIntent().getExtras().getString(BundleConstants.SEARCH_TYPE);
        animalListIntent.putExtra(BundleConstants.SEARCH_TYPE, searchType);

        Button adoptARescueButton = (Button) findViewById(R.id.adopt_a_rescue);
        Button weimaranaButton = (Button) findViewById(R.id.weimarana);
        Button littleFriendsButton = (Button) findViewById(R.id.little_friends);
        Button nevadaSPCAButton = (Button) findViewById(R.id.nevada_SPCA);
        Button secondChanceButton = (Button) findViewById(R.id.second_chance);
        Button southernNevadaButton = (Button) findViewById(R.id.southern_nevada_beagle);
        Button foundationButton = (Button) findViewById(R.id.foundation_button);
        Button hendoButton = (Button) findViewById(R.id.hendo_button);
        Button allButton = (Button) findViewById(R.id.all_button);

        if (searchType.equals(BundleConstants.SEARCH_ADOPT)) {
            adoptARescueButton.setVisibility(View.VISIBLE);
            weimaranaButton.setVisibility(View.VISIBLE);
            littleFriendsButton.setVisibility(View.VISIBLE);
            nevadaSPCAButton.setVisibility(View.VISIBLE);
            secondChanceButton.setVisibility(View.VISIBLE);
            southernNevadaButton.setVisibility(View.VISIBLE);
        } else if (searchType.equals(BundleConstants.SEARCH_LOST_AND_FOUND)) {
            adoptARescueButton.setVisibility(View.GONE);
            weimaranaButton.setVisibility(View.GONE);
            littleFriendsButton.setVisibility(View.GONE);
            nevadaSPCAButton.setVisibility(View.GONE);
            secondChanceButton.setVisibility(View.GONE);
            southernNevadaButton.setVisibility(View.GONE);
        }

        // get the animal type passed in from the previous activity and store it in the next activity
        String animalType = getIntent().getExtras().getString(BundleConstants.ANIMAL_TYPE);
        animalListIntent.putExtra(BundleConstants.ANIMAL_TYPE, animalType);

        adoptARescueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animalListIntent.putExtra(BundleConstants.SHELTER, Shelter.ADOPT_RESCUE_PET);
                startActivity(animalListIntent);
            }
        });
        weimaranaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animalListIntent.putExtra(BundleConstants.SHELTER, Shelter.WEIMARANER_CLUB);
                startActivity(animalListIntent);
            }
        });
        littleFriendsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animalListIntent.putExtra(BundleConstants.SHELTER, Shelter.LITTLE_FRIENDS);
                startActivity(animalListIntent);
            }
        });
        nevadaSPCAButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animalListIntent.putExtra(BundleConstants.SHELTER, Shelter.NEVADA_SPCA);
                startActivity(animalListIntent);
            }
        });
        secondChanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animalListIntent.putExtra(BundleConstants.SHELTER, Shelter.SECOND_CHANCE);
                startActivity(animalListIntent);
            }
        });
        southernNevadaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animalListIntent.putExtra(BundleConstants.SHELTER, Shelter.SOUTHERN_NEVADA_BEAGLE);
                startActivity(animalListIntent);
            }
        });

        foundationButton.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                animalListIntent.putExtra(BundleConstants.SHELTER, Shelter.ANIMAL_FOUNDATION);
                startActivity(animalListIntent);
            }
        });

        hendoButton.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                animalListIntent.putExtra(BundleConstants.SHELTER, Shelter.HENDERSON_CARE_AND_CONTROL);
                startActivity(animalListIntent);
            }
        });

        allButton.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                animalListIntent.putExtra(BundleConstants.SHELTER, Shelter.ALL_LOCATIONS);
                startActivity(animalListIntent);
            }
        });
    }
}
