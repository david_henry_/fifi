package com.hackathon.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.hackathon.android.App;
import com.hackathon.android.R;
import com.hackathon.android.model.Animal;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Dave on 12/5/13.
 */
public class AnimalsAdapter extends ArrayAdapter<Animal> {

    Context context;
    int layoutResourceId;

    public AnimalsAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId, new ArrayList<Animal>());
        this.layoutResourceId = layoutResourceId;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        AnimalHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new AnimalHolder();
            holder.img = (ImageView)row.findViewById(R.id.img);
            holder.name = (TextView)row.findViewById(R.id.name);
            holder.id = (TextView)row.findViewById(R.id.id);

            row.setTag(holder);
        }
        else
        {
            holder = (AnimalHolder)row.getTag();
        }

        Animal animal = super.getItem(position);

        holder.id.setText(animal.getId());

        if (animal.getName() == null) {
            holder.name.setText(animal.getBreed());
        } else {
            holder.name.setText(animal.getName());
        }

        // convert bitmap to bitmap drawable
        if (animal.getImage() != null) {
            holder.img.setImageDrawable(new BitmapDrawable(context.getResources(), animal.getImage()));
        }

        return row;
    }

    @Override
    public void add(Animal object) {
        super.add(object);
    }

    public void addImage(int position, InputStream image) {
        Animal animal = super.getItem(position);
        remove(animal);
        animal.setImage(image);
        insert(animal, position);
    }

    static class AnimalHolder
    {
        ImageView img;
        TextView name;
        public TextView id;
    }
}
