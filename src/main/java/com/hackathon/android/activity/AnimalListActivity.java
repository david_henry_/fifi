package com.hackathon.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.*;
import android.widget.*;
import com.hackathon.android.R;
import com.hackathon.android.adapter.AnimalsAdapter;
import com.hackathon.android.model.Animal;
import com.hackathon.android.model.AnimalList;
import com.hackathon.android.model.Shelter;
import com.hackathon.android.rest.AnimalRequest;
import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.simple.SmallBinaryRequest;

import java.io.InputStream;

public class AnimalListActivity extends OptionsBarActivity {

    private static final String KEY_LAST_REQUEST_CACHE_KEY = "lastRequestCacheKey";

    private SpiceManager spiceManager = new SpiceManager(
            JacksonSpringAndroidSpiceService.class);

    private AnimalsAdapter animalsAdapter;
    private ListView animalsList;

    private String lastRequestCacheKey;

    private Intent animalDetailsIntent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.animals_view);

        initUIComponents();
    }

    @Override
    protected void onStart() {
        spiceManager.start(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    private void initUIComponents() {
        animalsList = (ListView) findViewById(R.id.animals_list);
        animalsList.setDivider(null);
        animalsList.setVisibility(View.GONE);

        animalDetailsIntent = new Intent(this, AnimalDetailsActivity.class);

        animalsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // send the selected animal and the shelter to the animal details activity
                Shelter shelter = getIntent().getParcelableExtra(BundleConstants.SHELTER);
                Animal animal = animalsAdapter.getItem(position);

                //ensure we have a location selected
                if (shelter == null) {
                    shelter = Shelter.getShelterFromAnimal(animal.getLocation());
                }

                animalDetailsIntent.putExtra(BundleConstants.SHELTER, shelter);
                animalDetailsIntent.putExtra(BundleConstants.ANIMAL, animalsAdapter.getItem(position));

                startActivity(animalDetailsIntent);
            }
        });

        animalsAdapter = new AnimalsAdapter(this, R.layout.animals_list_row);
        animalsList.setAdapter(animalsAdapter);

        performSearchRequest();
    }

    private void performSearchRequest() {
        AnimalListActivity.this.setProgressBarIndeterminateVisibility(true);

        String animalType = getIntent().getExtras().getString(BundleConstants.ANIMAL_TYPE);
        String searchType = getIntent().getExtras().getString(BundleConstants.SEARCH_TYPE);

        // if Shelter is ALL_LOCATIONS this will be null
        Shelter shelter = getIntent().getParcelableExtra(BundleConstants.SHELTER);

        String location = null;
        if (shelter != null) {
            location = shelter.getName();
        }

        AnimalRequest request = new AnimalRequest(animalType, location, searchType);
        lastRequestCacheKey = request.createCacheKey();
        spiceManager.execute(request, lastRequestCacheKey,
                DurationInMillis.ONE_MINUTE, new ListAnimalsRequestListener());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (!TextUtils.isEmpty(lastRequestCacheKey)) {
            outState.putString(KEY_LAST_REQUEST_CACHE_KEY, lastRequestCacheKey);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(KEY_LAST_REQUEST_CACHE_KEY)) {
            lastRequestCacheKey = savedInstanceState
                    .getString(KEY_LAST_REQUEST_CACHE_KEY);
            spiceManager.addListenerIfPending(AnimalList.class,
                    lastRequestCacheKey, new ListAnimalsRequestListener());
            spiceManager.getFromCache(AnimalList.class,
                    lastRequestCacheKey, DurationInMillis.ONE_MINUTE,
                    new ListAnimalsRequestListener());
        }
    }

    private class ListAnimalsRequestListener implements
            RequestListener<AnimalList> {
        @Override
        public void onRequestFailure(SpiceException e) {
            Toast.makeText(AnimalListActivity.this,
                    "Error during request: " + e.getLocalizedMessage(), Toast.LENGTH_LONG)
                    .show();
            AnimalListActivity.this.setProgressBarIndeterminateVisibility(false);
        }


        @Override
        public void onRequestSuccess(AnimalList listAnimals) {

            // listAnimals could be null just if contentManager.getFromCache(...)
            // doesn't return anything.
            if (listAnimals == null) {
                return;
            }

            animalsAdapter.clear();

            for(Animal animal : listAnimals) {
                animalsAdapter.add(animal);
            }

            animalsAdapter.notifyDataSetChanged();

            ProgressBar loadingIndicator = (ProgressBar) findViewById(R.id.empty);
            loadingIndicator.setVisibility(View.GONE);

            animalsList.setVisibility(View.VISIBLE);

            // load the image for the animal
            for(int index = 0; index < listAnimals.size(); index++) {
                SmallBinaryRequest request = new SmallBinaryRequest(listAnimals.get(index).getImgUrl());
                spiceManager.execute(request, new AnimalImageRequestListener(index));
            }
        }
    }

    private class AnimalImageRequestListener implements RequestListener<InputStream> {

        private int position;

        public AnimalImageRequestListener(int position) {
            this.position = position;
        }

        @Override
        public void onRequestFailure(SpiceException e) {
            Toast.makeText(AnimalListActivity.this,
                    "Error during request: " + e.getLocalizedMessage(), Toast.LENGTH_LONG)
                    .show();
        }

        @Override
        public void onRequestSuccess(InputStream inputStream) {

            // inputStream could be null just if contentManager.getFromCache(...)
            // doesn't return anything.
            if (inputStream == null) {
                return;
            }

            animalsAdapter.addImage(position, inputStream);
            animalsAdapter.notifyDataSetChanged();
        }
    }
}

