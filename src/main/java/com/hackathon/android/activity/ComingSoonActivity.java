package com.hackathon.android.activity;

import android.os.Bundle;
import com.hackathon.android.R;

/**
 * Created by Dave on 12/5/13.
 */
public class ComingSoonActivity extends OptionsBarActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.coming_soon_view);
    }
}
