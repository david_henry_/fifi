package com.hackathon.android.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import com.hackathon.android.App;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.InputStream;

/**
 * Created by Dave on 12/4/13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Animal implements Parcelable {

    private String id;
    private String name;
    private String age;
    private String sex;
    private String fix;
    private String breed;
    private String arrived;
    private String location;
    private String imgUrl;
    private Bitmap image;

    public Animal() {
    }

    public Animal(Parcel in)
    {
        this.id = in.readString();
        this.name = in.readString();
        this.age = in.readString();
        this.sex = in.readString();
        this.fix = in.readString();
        this.breed = in.readString();
        this.arrived = in.readString();
        this.location = in.readString();
        this.imgUrl = in.readString();
        this.image = in.readParcelable(getClass().getClassLoader());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getFix() {
        return fix;
    }

    public void setFix(String fix) {
        this.fix = fix;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getArrived() {
        return arrived;
    }

    public void setArrived(String arrived) {
        this.arrived = arrived;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(InputStream image) {
        // save as a bitmap for parcelling
        this.image = BitmapFactory.decodeStream(image);
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(age);
        dest.writeString(sex);
        dest.writeString(fix);
        dest.writeString(breed);
        dest.writeString(arrived);
        dest.writeString(location);
        dest.writeString(imgUrl);
        dest.writeParcelable(image, flags);
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Animal createFromParcel(Parcel in)
        {
            return new Animal(in);
        }

        public Animal[] newArray(int size)
        {
            return new Animal[size];
        }
    };
}
