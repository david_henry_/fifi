package com.hackathon.android.sound;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import com.hackathon.android.App;
import com.hackathon.android.R;

/**
 * Created by Dave on 12/6/13.
 */
public class MediaPlayerHolder {

    private static MediaPlayerHolder MEDIA_PLAYER_HOLDER;

    private MediaPlayer dogMediaPlayer;
    private MediaPlayer catMediaPlayer;
    private MediaPlayer pigMediaPlayer;

    private MediaPlayerHolder() {
        dogMediaPlayer = MediaPlayer.create(App.context(), R.raw.dog_bark);
        catMediaPlayer = MediaPlayer.create(App.context(), R.raw.meow);
        pigMediaPlayer = MediaPlayer.create(App.context(), R.raw.snort);

        setMediaPlayersVolume();
    }

    public static synchronized MediaPlayerHolder getInstance() {
        if (MEDIA_PLAYER_HOLDER == null) {
            MEDIA_PLAYER_HOLDER = new MediaPlayerHolder();
        }
        return MEDIA_PLAYER_HOLDER;
    }

    public void setMediaPlayersVolume() {
        AudioManager am = (AudioManager) App.context().getSystemService(App.context().AUDIO_SERVICE);

        float volume = 1/15f*am.getStreamVolume(AudioManager.STREAM_RING);

        dogMediaPlayer.setVolume( volume, volume);
        catMediaPlayer.setVolume( volume, volume);
        pigMediaPlayer.setVolume( volume, volume);
    }

    public MediaPlayer getDogMediaPlayer() {
        return dogMediaPlayer;
    }

    public void setDogMediaPlayer(MediaPlayer dogMediaPlayer) {
        this.dogMediaPlayer = dogMediaPlayer;
    }

    public MediaPlayer getCatMediaPlayer() {
        return catMediaPlayer;
    }

    public void setCatMediaPlayer(MediaPlayer catMediaPlayer) {
        this.catMediaPlayer = catMediaPlayer;
    }

    public MediaPlayer getPigMediaPlayer() {
        return pigMediaPlayer;
    }

    public void setPigMediaPlayer(MediaPlayer pigMediaPlayer) {
        this.pigMediaPlayer = pigMediaPlayer;
    }
}
