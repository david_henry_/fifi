package com.hackathon.android.activity;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.hackathon.android.R;
import com.hackathon.android.model.Animal;
import com.hackathon.android.model.Shelter;

import java.util.Locale;

/**
 * Created by Dave on 12/5/13.
 */
public class AnimalDetailsActivity extends OptionsBarActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.animal_details_view);

        initUIComponents();
    }

    private void initUIComponents() {

        final Animal animal = getIntent().getParcelableExtra(BundleConstants.ANIMAL);
        final Shelter shelter = getIntent().getParcelableExtra(BundleConstants.SHELTER);

        TextView id = (TextView) findViewById(R.id.id);
        id.setText(animal.getId());

        TextView name = (TextView) findViewById(R.id.name);
        name.setText(animal.getName());

        TextView age = (TextView) findViewById(R.id.age);
        age.setText(animal.getAge());

        TextView sex = (TextView) findViewById(R.id.sex);
        sex.setText(animal.getSex());

        TextView fix = (TextView) findViewById(R.id.fix);
        fix.setText(animal.getFix());

        TextView breed = (TextView) findViewById(R.id.breed);
        breed.setText(animal.getBreed());

        TextView arrived = (TextView) findViewById(R.id.arrived);
        arrived.setText(animal.getArrived());

        Button location = (Button) findViewById(R.id.location);
        location.setText(animal.getLocation());

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (shelter.getLatitude() == null || shelter.getLongitude() == null) {
                    Toast.makeText(AnimalDetailsActivity.this, "No location stored", Toast.LENGTH_LONG).show();
                    return;
                }

                String uri = String.format(Locale.ENGLISH, "geo:%f,%f?z=%d&q=%f,%f (%s)",
                        shelter.getLatitude(),
                        shelter.getLongitude(),
                        10, // max 23
                        shelter.getLatitude(),
                        shelter.getLongitude(),
                        animal.getLocation());

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        });

        ImageView image = (ImageView) findViewById(R.id.image);
        image.setImageDrawable(new BitmapDrawable(getResources(), animal.getImage()));

        Button contact = (Button) findViewById(R.id.contact);
        contact.setText(shelter.getPhoneText());

        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + shelter.getPhone()));
                startActivity(callIntent);
            }
        });
    }
}
