package com.hackathon.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.hackathon.android.R;

/**
 * Created by Dave on 12/5/13.
 */
public class MainActivity extends Activity {

    private Intent searchIntent;
    private Intent registerIntent;
    private Intent soundBoardIntent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.main_view);
        setTitle(R.string.empty);

        initUIComponents();
    }

    private void initUIComponents() {

        searchIntent = new Intent(this, SearchActivity.class);
        registerIntent = new Intent(this, RegisterActivity.class);
        soundBoardIntent = new Intent(this, SoundBoardActivity.class);

        Button searchButton = (Button) findViewById(R.id.search_button);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(searchIntent);
            }
        });

        Button registerButton = (Button) findViewById(R.id.register_button);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(registerIntent);
            }
        });

        TextView searchLabel = (TextView) findViewById(R.id.search_label);

        searchLabel.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                startActivity(soundBoardIntent);
                return true;
            }
        });
    }

}
