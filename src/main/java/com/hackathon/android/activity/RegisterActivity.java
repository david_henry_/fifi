package com.hackathon.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.hackathon.android.R;

/**
 * Created by Dave on 12/7/13.
 */
public class RegisterActivity extends OptionsBarActivity {

    private Intent comingSoonIntent;
    private Intent registerFoundIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.register_view);

        initUIComponents();
    }

    private void initUIComponents() {

        comingSoonIntent = new Intent(this, ComingSoonActivity.class);
        registerFoundIntent = new Intent(this, RegisterFoundActivity.class);


        Button registerFoundButton = (Button) findViewById(R.id.register_found_button);

        registerFoundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(registerFoundIntent);
            }
        });

        Button registerLostButton = (Button) findViewById(R.id.register_lost_button);

        registerLostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(comingSoonIntent);
            }
        });
    }
}
