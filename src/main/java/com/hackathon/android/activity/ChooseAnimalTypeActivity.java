package com.hackathon.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import com.hackathon.android.R;
import com.hackathon.android.sound.MediaPlayerHolder;

/**
 * Created by Dave on 12/5/13.
 */
public class ChooseAnimalTypeActivity extends OptionsBarActivity {

    public static final String CAT = "cat";
    public static final String DOG = "dog";
    public static final String OTHER = "other";
    private Intent chooseLocationIntent;

    private MediaPlayerHolder mediaPlayerHolder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.choose_animal_type_view);

        mediaPlayerHolder = MediaPlayerHolder.getInstance();

        initUIComponents();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mediaPlayerHolder.setMediaPlayersVolume();
    }

    private void initUIComponents() {

        ImageView catButton = (ImageView) findViewById(R.id.cat_button);
        ImageView dogButton = (ImageView) findViewById(R.id.dog_button);
        ImageView otherButton = (ImageView) findViewById(R.id.other_button);

        chooseLocationIntent = new Intent(this, ChooseLocationActivity.class);

        // Get the search type passed in from the previous activity and store it in the next activity
        String searchType = getIntent().getExtras().getString(BundleConstants.SEARCH_TYPE);
        chooseLocationIntent.putExtra(BundleConstants.SEARCH_TYPE, searchType);

        catButton.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseLocationIntent.putExtra(BundleConstants.ANIMAL_TYPE, CAT);
                mediaPlayerHolder.getCatMediaPlayer().start();
                startActivity(chooseLocationIntent);
            }
        });

        dogButton.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseLocationIntent.putExtra(BundleConstants.ANIMAL_TYPE, DOG);
                mediaPlayerHolder.getDogMediaPlayer().start();
                startActivity(chooseLocationIntent);
            }
        });

        otherButton.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseLocationIntent.putExtra(BundleConstants.ANIMAL_TYPE, OTHER);
                mediaPlayerHolder.getPigMediaPlayer().start();
                startActivity(chooseLocationIntent);
            }
        });
    }



}
