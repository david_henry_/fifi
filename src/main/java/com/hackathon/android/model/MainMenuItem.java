package com.hackathon.android.model;

import android.content.Intent;

/**
 * Created by Dave on 12/5/13.
 */
public class MainMenuItem {

    private String title;
    private Intent intent;

    public MainMenuItem(String title, Intent intent) {
        this.title = title;
        this.intent = intent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }
}
