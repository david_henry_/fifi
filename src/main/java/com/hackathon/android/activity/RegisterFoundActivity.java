package com.hackathon.android.activity;

import android.os.Bundle;
import android.view.*;
import android.widget.Button;
import android.widget.Toast;
import com.hackathon.android.R;

/**
 * Created by Dave on 12/6/13.
 */
public class RegisterFoundActivity extends OptionsBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.register_found_view);

        initUIComponents();
    }

    private void initUIComponents() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Button registerButton = (Button) findViewById(R.id.register_animal);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(RegisterFoundActivity.this, "Currently Disabled", Toast.LENGTH_LONG)
                        .show();
            }
        });
    }
}
