package com.hackathon.android.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import com.hackathon.android.R;
import com.hackathon.android.sound.MediaPlayerHolder;

/**
 * Created by Dave on 12/6/13.
 */
public class SoundBoardActivity extends OptionsBarActivity {

    private MediaPlayerHolder mediaPlayerHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sound_board_view);

        mediaPlayerHolder = MediaPlayerHolder.getInstance();

        initUIComponents();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mediaPlayerHolder.setMediaPlayersVolume();
    }

    private void initUIComponents() {
        ImageButton catButton = (ImageButton) findViewById(R.id.cat_button);
        ImageButton dogButton = (ImageButton) findViewById(R.id.dog_button);
        ImageButton otherButton = (ImageButton) findViewById(R.id.pig_button);

        catButton.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayerHolder.getCatMediaPlayer().start();
            }
        });

        dogButton.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayerHolder.getDogMediaPlayer().start();
            }
        });

        otherButton.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayerHolder.getPigMediaPlayer().start();
            }
        });
    }
}
