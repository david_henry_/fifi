package com.hackathon.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.hackathon.android.R;

/**
 * Created by Dave on 12/7/13.
 */
public class SearchActivity extends OptionsBarActivity {

    private Intent searchLostAndFoundIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.search_view);

        initUIComponents();
    }

    private void initUIComponents() {

        searchLostAndFoundIntent = new Intent(this, ChooseAnimalTypeActivity.class);

        Button searchLostAndFound = (Button) findViewById(R.id.search_lost_and_found_button);

        searchLostAndFound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchLostAndFoundIntent.putExtra(BundleConstants.SEARCH_TYPE, BundleConstants.SEARCH_LOST_AND_FOUND);
                startActivity(searchLostAndFoundIntent);
            }
        });

        Button searchAdoptable = (Button) findViewById(R.id.search_adopt_button);

        searchAdoptable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchLostAndFoundIntent.putExtra(BundleConstants.SEARCH_TYPE, BundleConstants.SEARCH_ADOPT);
                startActivity(searchLostAndFoundIntent);
            }
        });
    }
}
