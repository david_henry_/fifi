package com.hackathon.android.activity;

/**
 * Created by Dave on 12/5/13.
 */
public class BundleConstants {
    public static final String ANIMAL_TYPE = "animalType";
    public static final String SHELTER = "location";
    public static final String ANIMAL = "animal";

    // search types
    public static final String SEARCH_TYPE = "searchType";
    public static final String SEARCH_ADOPT = "adopt";
    public static final String SEARCH_LOST_AND_FOUND = "lostAndFound";
}
