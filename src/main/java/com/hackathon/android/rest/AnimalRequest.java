package com.hackathon.android.rest;

import com.hackathon.android.activity.BundleConstants;
import com.hackathon.android.model.AnimalList;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import org.springframework.util.Assert;

import java.net.URI;

/**
 * Created by Dave on 12/4/13.
 */
public class AnimalRequest extends SpringAndroidSpiceRequest<AnimalList> {

    private String animalType;
    private String location;
    private String searchType;

    private static final String searchLostAndFoundPath = "/goaixxq/ce32698f79d94b3/sql/";
    private static final String searchAdoptablePath = "/bsjapty/1e5caf862b0a434/sql";

    public AnimalRequest(String animalType, String location, String searchType) {
        super(AnimalList.class);
        this.animalType = animalType;
        this.location = location;
        this.searchType = searchType;
    }

    @Override
    public AnimalList loadDataFromNetwork() throws Exception {
        String query = "q=select * from swdata";

        String path = "";
        if (searchType.equals(BundleConstants.SEARCH_LOST_AND_FOUND)) {
            path = searchLostAndFoundPath;
        } else if (searchType.equals(BundleConstants.SEARCH_ADOPT)) {
            path = searchAdoptablePath;
        }
        Assert.hasText(path);

        // TODO this is nasty
        if (animalType != null) {
            query += String.format(" where type='%s'", animalType);
        }

        // TODO more nasty
        if (location != null) {
            query += String.format(" and location='%s'", location);
        }

        URI uri = new URI("https", "free-ec2.scraperwiki.com", path, query, null);
        return getRestTemplate().getForObject(uri, AnimalList.class);

    }

    /**
     * This method generates a unique cache key for this request. In this case
     * our cache key depends just on the keyword.
     * @return
     */
    public String createCacheKey() {
        return "animals." + animalType + location;
    }
}
